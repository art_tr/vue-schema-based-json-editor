import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import * as common from 'schema-based-json-editor';
import { Icon } from './icon';
import { Optional } from './optional';
import { Description } from './description';
import { stringEditorTemplateHtml, stringEditorTemplateHtmlStatic } from './variables';
import 'markdown-tip-vue';
import 'select2-vue-component';
import 'file-uploader-vue-component';
var StringEditor = /** @class */ (function (_super) {
    __extends(StringEditor, _super);
    function StringEditor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.value = '';
        _this.errorMessage = '';
        _this.buttonGroupStyle = common.buttonGroupStyleString;
        _this.collapsed = false;
        _this.imagePreviewStyle = common.imagePreviewStyleString;
        return _this;
    }
    StringEditor.prototype.onChange = function (e) {
        this.value = e.target.value;
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    StringEditor.prototype.beforeMount = function () {
        this.value = common.getDefaultValue(this.required, this.schema, this.initialValue);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    StringEditor.prototype.mounted = function () {
        var _this = this;
        if (this.monacoEditor && this.$refs.monacoEditor) {
            this.monacoCodeEditor = this.monacoEditor.create(this.$refs.monacoEditor, {
                value: this.value,
                language: 'json',
                minimap: { enabled: false },
                lineNumbers: 'off'
            });
            var timer_1;
            this.monacoCodeEditor.onDidChangeModelContent(function () {
                clearTimeout(timer_1);
                timer_1 = setTimeout(function () {
                    _this.value = _this.monacoCodeEditor.getValue();
                    _this.validate();
                    _this.$emit('update-value', { value: _this.value, isValid: !_this.errorMessage });
                }, 500);
            });
        }
    };
    StringEditor.prototype.beforeDestroy = function () {
        if (this.monacoCodeEditor) {
            this.monacoCodeEditor.dispose();
        }
    };
    Object.defineProperty(StringEditor.prototype, "canPreviewImage", {
        get: function () {
            return common.isImageUrl(this.value) || common.isBase64Image(this.value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "canPreviewMarkdown", {
        get: function () {
            return this.md && this.schema.format === 'markdown';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "canPreviewCode", {
        get: function () {
            return this.hljs && this.schema.format === 'code';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "canPreview", {
        get: function () {
            return (!!this.value) && (this.canPreviewImage || this.canPreviewMarkdown || this.canPreviewCode);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "useTextArea", {
        get: function () {
            return this.value !== undefined
                && (this.schema.enum === undefined || this.isReadOnly)
                && (this.schema.format === 'textarea' || this.schema.format === 'code' || this.schema.format === 'json' || this.schema.format === 'markdown');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "useInput", {
        get: function () {
            return this.value !== undefined
                && !this.collapsed
                && (this.schema.enum === undefined || this.isReadOnly)
                && (this.schema.format !== 'textarea' && this.schema.format !== 'code' && this.schema.format !== 'json' && this.schema.format !== 'markdown');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "useSelect", {
        get: function () {
            return this.value !== undefined && this.schema.enum !== undefined && !this.isReadOnly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "useSelect2Component", {
        get: function () {
            return this.useSelect && !this.noSelect2 && this.schema.format !== 'select' && this.schema.format !== 'radiobox';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "useSelectComponent", {
        get: function () {
            return this.useSelect && (this.schema.format === 'select' || this.noSelect2);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "useRadioBoxComponent", {
        get: function () {
            return this.useSelect && this.schema.format === 'radiobox';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "getImageUrl", {
        get: function () {
            return this.forceHttps ? common.replaceProtocal(this.value) : this.value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "getMarkdown", {
        get: function () {
            return this.md.render(this.value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "getCode", {
        get: function () {
            return this.hljs.highlightAuto(this.value).value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "isReadOnly", {
        get: function () {
            return this.readonly || this.schema.readonly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "hasDeleteButtonFunction", {
        get: function () {
            return this.hasDeleteButton && !this.isReadOnly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "willPreviewImage", {
        get: function () {
            return this.value && !this.collapsed && this.canPreviewImage;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "willPreviewMarkdown", {
        get: function () {
            return this.value && !this.collapsed && this.canPreviewMarkdown;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "willPreviewCode", {
        get: function () {
            return this.value && !this.collapsed && this.canPreviewCode;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "titleToShow", {
        get: function () {
            return common.getTitle(this.title, this.schema.title);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "options", {
        get: function () {
            return common.getOptions(this.schema);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "canUpload", {
        get: function () {
            return this.schema.format === 'base64';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StringEditor.prototype, "className", {
        get: function () {
            var rowClass = this.errorMessage ? this.theme.errorRow : this.theme.row;
            return this.schema.className ? rowClass + ' ' + this.schema.className : rowClass;
        },
        enumerable: false,
        configurable: true
    });
    StringEditor.prototype.updateSelection = function (value) {
        this.value = value;
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    StringEditor.prototype.toggleOptional = function () {
        this.value = common.toggleOptional(this.value, this.schema, this.initialValue);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    StringEditor.prototype.collapseOrExpand = function () {
        this.collapsed = !this.collapsed;
    };
    StringEditor.prototype.fileGot = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            _this.value = reader.result;
            _this.validate();
            _this.$emit('update-value', { value: _this.value, isValid: !_this.errorMessage });
        };
        reader.onerror = function (error) {
            console.log(error);
        };
    };
    StringEditor.prototype.validate = function () {
        this.errorMessage = common.getErrorMessageOfString(this.value, this.schema, this.locale);
    };
    StringEditor = __decorate([
        Component({
            render: stringEditorTemplateHtml,
            staticRenderFns: stringEditorTemplateHtmlStatic,
            components: {
                icon: Icon,
                optional: Optional,
                description: Description
            },
            props: ['schema', 'initialValue', 'title', 'theme', 'icon', 'locale', 'readonly', 'required', 'hasDeleteButton', 'dragula', 'md', 'hljs', 'forceHttps', 'noSelect2', 'monacoEditor']
        })
    ], StringEditor);
    return StringEditor;
}(Vue));
export { StringEditor };
