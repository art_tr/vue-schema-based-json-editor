import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export * from 'schema-based-json-editor';
export declare class JSONEditor extends Vue {
    schema: common.Schema;
    theme?: string;
    locale: common.Locale;
    icon?: string;
    markdownit?: any;
    hljs?: common.HLJS;
    forceHttps?: boolean;
    get themeObject(): {
        card: string;
        row: string;
        errorRow: string;
        input: string;
        errorInput: string;
        textarea: string;
        errorTextarea: string;
        checkbox: string;
        radiobox: string;
        button: string;
        buttonGroup: string;
        title: string;
        description: string;
        select: string;
    };
    get localeObject(): {
        button: {
            collapse: string;
            expand: string;
            add: string;
            delete: string;
        };
        error: {
            minLength: string;
            maxLength: string;
            pattern: string;
            minimum: string;
            maximum: string;
            largerThan: string;
            smallerThan: string;
            minItems: string;
            uniqueItems: string;
            multipleOf: string;
            minProperties: string;
            maxProperties: string;
        };
        info: {
            notExists: string;
            true: string;
            false: string;
            search: string;
        };
        markdownTipLocale: any;
        fileUploaderLocale: {
            dragAndDrop: string;
            selectFile: string;
            pasteFromClipboard: string;
        };
    };
    get iconObject(): {
        isText: boolean;
        collapse: string;
        expand: string;
        add: string;
        delete: string;
    };
    get md(): any;
    getReference: (name: string) => common.BooleanSchema | common.ObjectSchema | common.ArraySchema | common.NumberSchema | common.StringSchema | common.NullSchema | common.AnySchema | undefined;
    updateValue(value: common.ValueType): void;
}
