import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export declare class Optional extends Vue {
    required?: boolean;
    value?: common.ValueType;
    isReadOnly?: boolean;
    theme: common.Theme;
    locale: common.Locale;
    get hasOptionalCheckbox(): boolean;
}
