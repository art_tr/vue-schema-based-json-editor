import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export declare class ObjectEditor extends Vue {
    schema: common.ObjectSchema;
    initialValue?: {
        [name: string]: common.ValueType;
    };
    title: string;
    icon: common.Icon;
    locale: common.Locale;
    theme: common.Theme;
    readonly: boolean;
    required: boolean;
    hasDeleteButton: boolean;
    minItemCountIfNeedFilter: boolean;
    getReference: (name: string) => common.Schema | undefined;
    collapsed?: boolean | undefined;
    value?: {
        [name: string]: common.ValueType;
    };
    buttonGroupStyle: string;
    errorMessage?: string | undefined;
    filter: string;
    private invalidProperties;
    private properties;
    private watchedProperties;
    beforeMount(): void;
    get filteredProperties(): {
        property: string;
        schema: common.Schema;
        propertyName: string;
    }[];
    get isReadOnly(): boolean | undefined;
    get hasDeleteButtonFunction(): boolean;
    get titleToShow(): string;
    get showFilter(): boolean;
    get className(): string;
    isRequired(property: string): boolean | undefined;
    collapseOrExpand(): void;
    toggleOptional(): void;
    onChange(property: string, { value, isValid }: common.ValidityValue<common.ValueType>): void;
    onFilterChange(e: {
        target: {
            value: string;
        };
    }): void;
    private validate;
}
