import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export declare class ArrayEditor extends Vue {
    schema: common.ArraySchema;
    initialValue?: common.ValueType[];
    title: string;
    icon: common.Icon;
    locale: common.Locale;
    theme: common.Theme;
    readonly: boolean;
    required: boolean;
    hasDeleteButton: boolean;
    dragula?: common.Dragula;
    noSelect2?: boolean;
    minItemCountIfNeedFilter?: number;
    getReference: (name: string) => common.Schema | undefined;
    renderSwitch: number;
    collapsed?: boolean | undefined;
    value?: common.ValueType[];
    errorMessage?: string | undefined;
    buttonGroupStyleString: string;
    buttonGroupBottomStyleString: string;
    filter: string;
    private invalidIndexes;
    private drak?;
    beforeMount(): void;
    get filteredValues(): {
        p: common.ValueType;
        i: number;
    }[];
    private get getValue();
    get isReadOnly(): boolean | undefined;
    get hasDeleteButtonFunction(): boolean;
    get hasAddButton(): boolean;
    get titleToShow(): string;
    get showFilter(): boolean;
    get className(): string;
    get options(): {
        value: string | number;
        label: string | number;
    }[];
    beforeDestroy(): void;
    mounted(): void;
    collapseOrExpand(): void;
    toggleOptional(): void;
    addItem(): void;
    onDeleteFunction(i: number): void;
    onChange(i: number, { value, isValid }: common.ValidityValue<common.ValueType>): void;
    onFilterChange(e: {
        target: {
            value: string;
        };
    }): void;
    isChecked(value: any): boolean | undefined;
    onChangeCheckbox(value: any): void;
    onChangeSelect2(value: any): void;
    private validate;
}
