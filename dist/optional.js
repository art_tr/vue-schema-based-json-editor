import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import { optionalTemplateHtml, optionalTemplateHtmlStatic } from './variables';
var Optional = /** @class */ (function (_super) {
    __extends(Optional, _super);
    function Optional() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Optional.prototype, "hasOptionalCheckbox", {
        get: function () {
            return !this.required && (this.value === undefined || !this.isReadOnly);
        },
        enumerable: false,
        configurable: true
    });
    Optional = __decorate([
        Component({
            render: optionalTemplateHtml,
            staticRenderFns: optionalTemplateHtmlStatic,
            props: ['required', 'value', 'isReadOnly', 'theme', 'locale']
        })
    ], Optional);
    return Optional;
}(Vue));
export { Optional };
