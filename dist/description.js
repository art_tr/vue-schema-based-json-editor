import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import { descriptionTemplateHtml, descriptionTemplateHtmlStatic } from './variables';
var Description = /** @class */ (function (_super) {
    __extends(Description, _super);
    function Description() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Description = __decorate([
        Component({
            render: descriptionTemplateHtml,
            staticRenderFns: descriptionTemplateHtmlStatic,
            props: ['theme', 'message']
        })
    ], Description);
    return Description;
}(Vue));
export { Description };
