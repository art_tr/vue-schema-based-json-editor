declare const _default: {
    base: string;
    files: string[];
    handler: (file: string) => {
        type: string;
        name: string;
        path: string;
    } | {
        type: string;
        name?: undefined;
        path?: undefined;
    };
    out: string;
};
export default _default;
