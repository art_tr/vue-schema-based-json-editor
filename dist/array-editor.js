import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import * as common from 'schema-based-json-editor';
import { Icon } from './icon';
import { Editor } from './editor';
import { Optional } from './optional';
import { Description } from './description';
import { arrayEditorTemplateHtml, arrayEditorTemplateHtmlStatic } from './variables';
var ArrayEditor = /** @class */ (function (_super) {
    __extends(ArrayEditor, _super);
    function ArrayEditor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.renderSwitch = 1;
        _this.collapsed = false;
        _this.value = [];
        _this.errorMessage = '';
        _this.buttonGroupStyleString = common.buttonGroupStyleString;
        _this.buttonGroupBottomStyleString = "margin-left: 10px;margin-top: -20px;display: flex;justify-content: flex-end;";
        _this.filter = '';
        _this.invalidIndexes = [];
        _this.drak = null;
        return _this;
    }
    ArrayEditor.prototype.beforeMount = function () {
        this.collapsed = this.schema.collapsed;
        this.value = common.getDefaultValue(this.required, this.schema, this.initialValue);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    Object.defineProperty(ArrayEditor.prototype, "filteredValues", {
        get: function () {
            var _this = this;
            return this.getValue.map(function (p, i) { return ({ p: p, i: i }); })
                .filter(function (_a) {
                var p = _a.p, i = _a.i;
                return common.filterArray(p, i, _this.schema.items, _this.filter);
            });
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "getValue", {
        get: function () {
            if (this.value !== undefined && !this.collapsed) {
                return this.value;
            }
            return [];
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "isReadOnly", {
        get: function () {
            return this.readonly || this.schema.readonly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "hasDeleteButtonFunction", {
        get: function () {
            return this.hasDeleteButton && !this.isReadOnly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "hasAddButton", {
        get: function () {
            return !this.isReadOnly && this.value !== undefined && !this.schema.enum;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "titleToShow", {
        get: function () {
            return common.getTitle(this.title, this.schema.title);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "showFilter", {
        get: function () {
            var minItemCountIfNeedFilter = typeof this.minItemCountIfNeedFilter === 'number' ? this.minItemCountIfNeedFilter : common.minItemCountIfNeedFilter;
            return this.getValue.length >= minItemCountIfNeedFilter;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "className", {
        get: function () {
            var rowClass = this.errorMessage ? this.theme.errorRow : this.theme.row;
            return this.schema.className ? rowClass + ' ' + this.schema.className : rowClass;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ArrayEditor.prototype, "options", {
        get: function () {
            return common.getOptions(this.schema);
        },
        enumerable: false,
        configurable: true
    });
    ArrayEditor.prototype.beforeDestroy = function () {
        if (this.drak) {
            this.drak.destroy();
        }
    };
    ArrayEditor.prototype.mounted = function () {
        var _this = this;
        if (this.dragula) {
            var container = common.findContainer(this.$el.childNodes);
            if (container) {
                this.drak = this.dragula([container]);
                this.drak.on('drop', function (el, target, source, sibling) {
                    if (_this.value) {
                        common.switchItem(_this.value, el, sibling);
                        _this.renderSwitch = -_this.renderSwitch;
                        _this.$emit('update-value', { value: _this.value, isValid: !_this.errorMessage && _this.invalidIndexes.length === 0 });
                    }
                });
            }
        }
    };
    ArrayEditor.prototype.collapseOrExpand = function () {
        this.collapsed = !this.collapsed;
    };
    ArrayEditor.prototype.toggleOptional = function () {
        this.value = common.toggleOptional(this.value, this.schema, this.initialValue);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage && this.invalidIndexes.length === 0 });
    };
    ArrayEditor.prototype.addItem = function () {
        this.value.push(common.getDefaultValue(true, this.schema.items, undefined));
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage && this.invalidIndexes.length === 0 });
    };
    ArrayEditor.prototype.onDeleteFunction = function (i) {
        this.value.splice(i, 1);
        this.renderSwitch = -this.renderSwitch;
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage && this.invalidIndexes.length === 0 });
    };
    ArrayEditor.prototype.onChange = function (i, _a) {
        var value = _a.value, isValid = _a.isValid;
        this.value[i] = value;
        this.validate();
        common.recordInvalidIndexesOfArray(this.invalidIndexes, isValid, i);
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage && this.invalidIndexes.length === 0 });
    };
    ArrayEditor.prototype.onFilterChange = function (e) {
        this.filter = e.target.value;
    };
    ArrayEditor.prototype.isChecked = function (value) {
        return this.value && this.value.indexOf(value) !== -1;
    };
    ArrayEditor.prototype.onChangeCheckbox = function (value) {
        if (this.value) {
            var index = this.value.indexOf(value);
            if (index !== -1) {
                this.value.splice(index, 1);
            }
            else {
                this.value.push(value);
            }
            this.validate();
            this.$emit('update-value', { value: this.value, isValid: !this.errorMessage && this.invalidIndexes.length === 0 });
        }
    };
    ArrayEditor.prototype.onChangeSelect2 = function (value) {
        this.value = value;
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage && this.invalidIndexes.length === 0 });
    };
    ArrayEditor.prototype.validate = function () {
        this.errorMessage = common.getErrorMessageOfArray(this.value, this.schema, this.locale);
    };
    ArrayEditor = __decorate([
        Component({
            render: arrayEditorTemplateHtml,
            staticRenderFns: arrayEditorTemplateHtmlStatic,
            components: {
                icon: Icon,
                optional: Optional,
                description: Description,
                editor: Editor
            },
            props: [
                'schema',
                'initialValue',
                'title',
                'getReference',
                'theme',
                'icon',
                'locale',
                'readonly',
                'required',
                'hasDeleteButton',
                'dragula',
                'md',
                'hljs',
                'forceHttps',
                'disableCollapse',
                'noSelect2',
                'minItemCountIfNeedFilter',
                'monacoEditor'
            ]
        })
    ], ArrayEditor);
    return ArrayEditor;
}(Vue));
export { ArrayEditor };
