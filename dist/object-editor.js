import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import * as common from 'schema-based-json-editor';
import { Icon } from './icon';
import { Optional } from './optional';
import { Description } from './description';
import { Editor } from './editor';
import { objectEditorTemplateHtml, objectEditorTemplateHtmlStatic } from './variables';
var ObjectEditor = /** @class */ (function (_super) {
    __extends(ObjectEditor, _super);
    function ObjectEditor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.collapsed = false;
        _this.value = {};
        _this.buttonGroupStyle = common.buttonGroupStyleString;
        _this.errorMessage = '';
        _this.filter = '';
        _this.invalidProperties = [];
        _this.properties = [];
        _this.watchedProperties = [];
        return _this;
    }
    ObjectEditor.prototype.beforeMount = function () {
        this.collapsed = this.schema.collapsed;
        this.value = common.getDefaultValue(this.required, this.schema, this.initialValue);
        this.validate();
        if (this.value !== undefined) {
            var _loop_1 = function (property) {
                if (this_1.schema.properties.hasOwnProperty(property)) {
                    var schema = this_1.schema.properties[property];
                    var propertyName = schema.propertyName || property;
                    if (this_1.isRequired(property) !== false) {
                        var required = this_1.schema.required && this_1.schema.required.some(function (r) { return r === property; });
                        this_1.value[propertyName] = common.getDefaultValue(required, schema, this_1.value[propertyName]);
                    }
                    this_1.properties.push({
                        propertyName: propertyName,
                        property: property,
                        schema: schema
                    });
                }
            };
            var this_1 = this;
            for (var property in this.schema.properties) {
                _loop_1(property);
            }
            this.properties.sort(common.compare);
        }
        for (var property in this.schema.properties) {
            var schema = this.schema.properties[property];
            if (schema && schema.requiredWhen && !this.watchedProperties.includes(schema.requiredWhen[0])) {
                this.watchedProperties.push(schema.requiredWhen[0]);
            }
        }
        this.$emit('update-value', { value: this.value, isValid: true });
    };
    Object.defineProperty(ObjectEditor.prototype, "filteredProperties", {
        get: function () {
            var _this = this;
            return this.properties.filter(function (p) { return common.filterObject(p, _this.filter); });
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ObjectEditor.prototype, "isReadOnly", {
        get: function () {
            return this.readonly || this.schema.readonly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ObjectEditor.prototype, "hasDeleteButtonFunction", {
        get: function () {
            return this.hasDeleteButton && !this.isReadOnly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ObjectEditor.prototype, "titleToShow", {
        get: function () {
            if (this.hasDeleteButton) {
                return common.getTitle(common.findTitle(this.value, this.properties), this.title, this.schema.title);
            }
            return common.getTitle(this.title, this.schema.title);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ObjectEditor.prototype, "showFilter", {
        get: function () {
            var _this = this;
            var propertyCount = this.properties.filter(function (p) { return _this.isRequired(p.property) !== false; }).length;
            var minItemCountIfNeedFilter = typeof this.minItemCountIfNeedFilter === 'number' ? this.minItemCountIfNeedFilter : common.minItemCountIfNeedFilter;
            return propertyCount >= minItemCountIfNeedFilter;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ObjectEditor.prototype, "className", {
        get: function () {
            var rowClass = this.errorMessage ? this.theme.errorRow : this.theme.row;
            return this.schema.className ? rowClass + ' ' + this.schema.className : rowClass;
        },
        enumerable: false,
        configurable: true
    });
    ObjectEditor.prototype.isRequired = function (property) {
        return common.isRequired(this.schema.required, this.value, this.schema, property);
    };
    ObjectEditor.prototype.collapseOrExpand = function () {
        this.collapsed = !this.collapsed;
    };
    ObjectEditor.prototype.toggleOptional = function () {
        this.value = common.toggleOptional(this.value, this.schema, this.initialValue);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: this.invalidProperties.length === 0 });
    };
    ObjectEditor.prototype.onChange = function (property, _a) {
        var value = _a.value, isValid = _a.isValid;
        this.value[property] = value;
        for (var p in this.schema.properties) {
            if (this.isRequired(p) === false) {
                this.value[p] = undefined;
            }
        }
        this.validate();
        if (this.watchedProperties.some(function (p) { return p === property; })) {
            this.$forceUpdate();
        }
        common.recordInvalidPropertiesOfObject(this.invalidProperties, isValid, property);
        this.$emit('update-value', { value: this.value, isValid: this.invalidProperties.length === 0 });
    };
    ObjectEditor.prototype.onFilterChange = function (e) {
        this.filter = e.target.value;
    };
    ObjectEditor.prototype.validate = function () {
        this.errorMessage = common.getErrorMessageOfObject(this.value, this.schema, this.locale);
    };
    ObjectEditor = __decorate([
        Component({
            render: objectEditorTemplateHtml,
            staticRenderFns: objectEditorTemplateHtmlStatic,
            components: {
                icon: Icon,
                optional: Optional,
                description: Description,
                editor: Editor
            },
            props: [
                'schema',
                'initialValue',
                'title',
                'getReference',
                'theme',
                'icon',
                'locale',
                'readonly',
                'required',
                'hasDeleteButton',
                'dragula',
                'md',
                'hljs',
                'forceHttps',
                'disableCollapse',
                'noSelect2',
                'minItemCountIfNeedFilter',
                'monacoEditor'
            ]
        })
    ], ObjectEditor);
    return ObjectEditor;
}(Vue));
export { ObjectEditor };
