import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import { iconTemplateHtml, iconTemplateHtmlStatic } from './variables';
var Icon = /** @class */ (function (_super) {
    __extends(Icon, _super);
    function Icon() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Icon = __decorate([
        Component({
            render: iconTemplateHtml,
            staticRenderFns: iconTemplateHtmlStatic,
            props: ['icon', 'text', 'theme']
        })
    ], Icon);
    return Icon;
}(Vue));
export { Icon };
