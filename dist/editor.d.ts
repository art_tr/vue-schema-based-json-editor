import Vue from 'vue';
import { Schema } from '.';
export declare class Editor extends Vue {
    schema: Schema;
    getReference: (name: string) => Schema | undefined;
    get realSchema(): Schema;
}
