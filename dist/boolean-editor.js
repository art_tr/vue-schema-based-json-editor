import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import * as common from 'schema-based-json-editor';
import { Icon } from './icon';
import { Optional } from './optional';
import { Description } from './description';
import { booleanEditorTemplateHtml, booleanEditorTemplateHtmlStatic } from './variables';
var BooleanEditor = /** @class */ (function (_super) {
    __extends(BooleanEditor, _super);
    function BooleanEditor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.value = false;
        _this.buttonGroupStyle = common.buttonGroupStyleString;
        return _this;
    }
    BooleanEditor.prototype.beforeMount = function () {
        this.value = common.getDefaultValue(this.required, this.schema, this.initialValue);
        this.$emit('update-value', { value: this.value, isValid: true });
    };
    Object.defineProperty(BooleanEditor.prototype, "isReadOnly", {
        get: function () {
            return this.readonly || this.schema.readonly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BooleanEditor.prototype, "hasDeleteButtonFunction", {
        get: function () {
            return this.hasDeleteButton && !this.isReadOnly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BooleanEditor.prototype, "titleToShow", {
        get: function () {
            return common.getTitle(this.title, this.schema.title);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BooleanEditor.prototype, "booleanOptions", {
        get: function () {
            return [
                {
                    value: true,
                    label: this.locale.info.true
                },
                {
                    value: false,
                    label: this.locale.info.false
                }
            ];
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BooleanEditor.prototype, "className", {
        get: function () {
            var rowClass = this.theme.row;
            return this.schema.className ? rowClass + ' ' + this.schema.className : rowClass;
        },
        enumerable: false,
        configurable: true
    });
    BooleanEditor.prototype.onChange = function () {
        this.value = !this.value;
        this.$emit('update-value', { value: this.value, isValid: true });
    };
    BooleanEditor.prototype.toggleOptional = function () {
        this.value = common.toggleOptional(this.value, this.schema, this.initialValue);
        this.$emit('update-value', { value: this.value, isValid: true });
    };
    BooleanEditor = __decorate([
        Component({
            render: booleanEditorTemplateHtml,
            staticRenderFns: booleanEditorTemplateHtmlStatic,
            components: {
                icon: Icon,
                optional: Optional,
                description: Description
            },
            props: ['schema', 'initialValue', 'title', 'theme', 'icon', 'locale', 'readonly', 'required', 'hasDeleteButton']
        })
    ], BooleanEditor);
    return BooleanEditor;
}(Vue));
export { BooleanEditor };
