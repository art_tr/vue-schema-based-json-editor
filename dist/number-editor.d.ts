import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export declare class NumberEditor extends Vue {
    schema: common.NumberSchema;
    initialValue?: number;
    title: string;
    theme: common.Theme;
    icon: common.Icon;
    locale: common.Locale;
    readonly: boolean;
    required: boolean;
    hasDeleteButton: boolean;
    noSelect2: boolean;
    value?: number | undefined;
    errorMessage?: string | undefined;
    buttonGroupStyle: string;
    onChange(e: {
        target: {
            value: string;
        };
    }): void;
    beforeMount(): void;
    get useInput(): boolean | undefined;
    private get useSelect();
    get useSelect2Component(): boolean;
    get useSelectComponent(): boolean;
    get useRadioBoxComponent(): boolean;
    get isReadOnly(): boolean | undefined;
    get hasDeleteButtonFunction(): boolean;
    get titleToShow(): string;
    get options(): {
        value: string | number;
        label: string | number;
    }[];
    get className(): string;
    updateSelection(value: number): void;
    toggleOptional(): void;
    private validate;
}
