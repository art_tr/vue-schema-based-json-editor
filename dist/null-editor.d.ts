import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export declare class NullEditor extends Vue {
    schema: common.ArraySchema;
    initialValue?: null;
    title: string;
    theme: common.Theme;
    icon: common.Icon;
    locale: common.Locale;
    readonly: boolean;
    required: boolean;
    hasDeleteButton: boolean;
    value?: null | undefined;
    buttonGroupStyle: string;
    beforeMount(): void;
    get isReadOnly(): boolean | undefined;
    get hasDeleteButtonFunction(): boolean;
    get titleToShow(): string;
    toggleOptional(): void;
}
