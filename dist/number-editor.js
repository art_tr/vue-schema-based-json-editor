import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import * as common from 'schema-based-json-editor';
import { Icon } from './icon';
import { Optional } from './optional';
import { Description } from './description';
import { numberEditorTemplateHtml, numberEditorTemplateHtmlStatic } from './variables';
var NumberEditor = /** @class */ (function (_super) {
    __extends(NumberEditor, _super);
    function NumberEditor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.value = 0;
        _this.errorMessage = '';
        _this.buttonGroupStyle = common.buttonGroupStyleString;
        return _this;
    }
    NumberEditor.prototype.onChange = function (e) {
        this.value = this.schema.type === 'integer' ? common.toInteger(e.target.value) : common.toNumber(e.target.value);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    NumberEditor.prototype.beforeMount = function () {
        this.value = common.getDefaultValue(this.required, this.schema, this.initialValue);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    Object.defineProperty(NumberEditor.prototype, "useInput", {
        get: function () {
            return this.value !== undefined && (this.schema.enum === undefined || this.isReadOnly);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "useSelect", {
        get: function () {
            return this.value !== undefined && (this.schema.enum !== undefined && !this.isReadOnly);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "useSelect2Component", {
        get: function () {
            return this.useSelect && !this.noSelect2 && this.schema.format !== 'select' && this.schema.format !== 'radiobox';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "useSelectComponent", {
        get: function () {
            return this.useSelect && (this.schema.format === 'select' || this.noSelect2);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "useRadioBoxComponent", {
        get: function () {
            return this.useSelect && this.schema.format === 'radiobox';
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "isReadOnly", {
        get: function () {
            return this.readonly || this.schema.readonly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "hasDeleteButtonFunction", {
        get: function () {
            return this.hasDeleteButton && !this.isReadOnly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "titleToShow", {
        get: function () {
            return common.getTitle(this.title, this.schema.title);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "options", {
        get: function () {
            return common.getOptions(this.schema);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(NumberEditor.prototype, "className", {
        get: function () {
            var rowClass = this.errorMessage ? this.theme.errorRow : this.theme.row;
            return this.schema.className ? rowClass + ' ' + this.schema.className : rowClass;
        },
        enumerable: false,
        configurable: true
    });
    NumberEditor.prototype.updateSelection = function (value) {
        this.value = value;
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    NumberEditor.prototype.toggleOptional = function () {
        this.value = common.toggleOptional(this.value, this.schema, this.initialValue);
        this.validate();
        this.$emit('update-value', { value: this.value, isValid: !this.errorMessage });
    };
    NumberEditor.prototype.validate = function () {
        this.errorMessage = common.getErrorMessageOfNumber(this.value, this.schema, this.locale);
    };
    NumberEditor = __decorate([
        Component({
            render: numberEditorTemplateHtml,
            staticRenderFns: numberEditorTemplateHtmlStatic,
            components: {
                icon: Icon,
                optional: Optional,
                description: Description
            },
            props: ['schema', 'initialValue', 'title', 'theme', 'icon', 'locale', 'readonly', 'required', 'hasDeleteButton', 'noSelect2']
        })
    ], NumberEditor);
    return NumberEditor;
}(Vue));
export { NumberEditor };
