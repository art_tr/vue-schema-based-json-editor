import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import * as common from 'schema-based-json-editor';
export * from 'schema-based-json-editor';
import { Editor } from './editor';
import { ArrayEditor } from './array-editor';
import { BooleanEditor } from './boolean-editor';
import { NullEditor } from './null-editor';
import { NumberEditor } from './number-editor';
import { ObjectEditor } from './object-editor';
import { StringEditor } from './string-editor';
import { AnyEditor } from './any-editor';
Vue.component('array-editor', ArrayEditor);
Vue.component('boolean-editor', BooleanEditor);
Vue.component('null-editor', NullEditor);
Vue.component('number-editor', NumberEditor);
Vue.component('object-editor', ObjectEditor);
Vue.component('string-editor', StringEditor);
Vue.component('any-editor', AnyEditor);
import { indexTemplateHtml, indexTemplateHtmlStatic } from './variables';
var JSONEditor = /** @class */ (function (_super) {
    __extends(JSONEditor, _super);
    function JSONEditor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.getReference = function (name) {
            if (_this.schema.definitions) {
                return _this.schema.definitions[name.substring('#/definitions/'.length)];
            }
            return undefined;
        };
        return _this;
    }
    Object.defineProperty(JSONEditor.prototype, "themeObject", {
        get: function () {
            return common.getTheme(this.theme);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(JSONEditor.prototype, "localeObject", {
        get: function () {
            return common.getLocale(this.locale);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(JSONEditor.prototype, "iconObject", {
        get: function () {
            return common.getIcon(this.icon, this.localeObject);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(JSONEditor.prototype, "md", {
        get: function () {
            return common.initializeMarkdown(this.markdownit, this.hljs, this.forceHttps);
        },
        enumerable: false,
        configurable: true
    });
    JSONEditor.prototype.updateValue = function (value) {
        this.$emit('update-value', value);
    };
    JSONEditor = __decorate([
        Component({
            render: indexTemplateHtml,
            staticRenderFns: indexTemplateHtmlStatic,
            components: {
                editor: Editor
            },
            props: [
                'schema',
                'initialValue',
                'theme',
                'icon',
                'locale',
                'readonly',
                'dragula',
                'markdownit',
                'hljs',
                'forceHttps',
                'disableCollapse',
                'noSelect2',
                'minItemCountIfNeedFilter',
                'monacoEditor'
            ]
        })
    ], JSONEditor);
    return JSONEditor;
}(Vue));
export { JSONEditor };
Vue.component('json-editor', JSONEditor);
