import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import { editorTemplateHtml, editorTemplateHtmlStatic } from './variables';
var Editor = /** @class */ (function (_super) {
    __extends(Editor, _super);
    function Editor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Editor_1 = Editor;
    Object.defineProperty(Editor.prototype, "realSchema", {
        get: function () {
            if (this.schema.$ref) {
                var reference = this.getReference(this.schema.$ref);
                if (reference) {
                    return reference;
                }
            }
            return this.schema;
        },
        enumerable: false,
        configurable: true
    });
    var Editor_1;
    Editor = Editor_1 = __decorate([
        Component({
            render: editorTemplateHtml,
            staticRenderFns: editorTemplateHtmlStatic,
            components: {
                editor: Editor_1
            },
            props: [
                'schema',
                'initialValue',
                'title',
                'getReference',
                'theme',
                'icon',
                'locale',
                'readonly',
                'required',
                'hasDeleteButton',
                'dragula',
                'md',
                'hljs',
                'forceHttps',
                'disableCollapse',
                'noSelect2',
                'minItemCountIfNeedFilter',
                'monacoEditor'
            ]
        })
    ], Editor);
    return Editor;
}(Vue));
export { Editor };
