import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export declare class BooleanEditor extends Vue {
    schema: common.BooleanSchema;
    initialValue?: boolean;
    title: string;
    theme: common.Theme;
    icon: common.Icon;
    locale: common.Locale;
    readonly: boolean;
    required: boolean;
    hasDeleteButton: boolean;
    value?: boolean | undefined;
    buttonGroupStyle: string;
    beforeMount(): void;
    get isReadOnly(): boolean | undefined;
    get hasDeleteButtonFunction(): boolean;
    get titleToShow(): string;
    get booleanOptions(): {
        value: boolean;
        label: string;
    }[];
    get className(): string;
    onChange(): void;
    toggleOptional(): void;
}
