import { __decorate, __extends } from "tslib";
import Vue from 'vue';
import Component from 'vue-class-component';
import * as common from 'schema-based-json-editor';
import JSON5 from 'json5';
import { Icon } from './icon';
import { Editor } from './editor';
import { Optional } from './optional';
import { Description } from './description';
import { anyEditorTemplateHtml, anyEditorTemplateHtmlStatic } from './variables';
var AnyEditor = /** @class */ (function (_super) {
    __extends(AnyEditor, _super);
    function AnyEditor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.toggleOptional = function () {
            _this.value = common.toggleOptional(_this.value, _this.schema, _this.initialValue);
            _this.$emit('update-value', { value: _this.value, isValid: true });
        };
        return _this;
    }
    AnyEditor.prototype.beforeMount = function () {
        this.value = common.getDefaultValue(this.required, this.schema, this.initialValue);
    };
    AnyEditor.prototype.mounted = function () {
        var _this = this;
        if (this.monacoEditor && this.$refs.monacoEditor) {
            this.monacoCodeEditor = this.monacoEditor.create(this.$refs.monacoEditor, {
                value: 'export default ' + JSON5.stringify(this.value, null, 2),
                language: 'javascript',
                minimap: { enabled: false },
                lineNumbers: 'off'
            });
            var timer_1;
            this.monacoCodeEditor.onDidChangeModelContent(function () {
                clearTimeout(timer_1);
                timer_1 = setTimeout(function () {
                    try {
                        var value = _this.monacoCodeEditor.getValue();
                        if (value.indexOf('export default') === 0) {
                            value = value.substring('export default'.length);
                        }
                        _this.value = JSON5.parse(value);
                        _this.$emit('update-value', { value: _this.value, isValid: true });
                    }
                    catch (_a) {
                        // do nothing
                    }
                }, 500);
            });
        }
    };
    Object.defineProperty(AnyEditor.prototype, "className", {
        get: function () {
            var rowClass = this.theme.row;
            return this.schema.className ? rowClass + ' ' + this.schema.className : rowClass;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AnyEditor.prototype, "titleToShow", {
        get: function () {
            return common.getTitle(this.title, this.schema.title);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AnyEditor.prototype, "isReadOnly", {
        get: function () {
            return this.readonly || this.schema.readonly;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AnyEditor.prototype, "hasDeleteButtonFunction", {
        get: function () {
            return this.hasDeleteButton && !this.isReadOnly;
        },
        enumerable: false,
        configurable: true
    });
    AnyEditor = __decorate([
        Component({
            render: anyEditorTemplateHtml,
            staticRenderFns: anyEditorTemplateHtmlStatic,
            components: {
                icon: Icon,
                optional: Optional,
                description: Description,
                editor: Editor
            },
            props: [
                'schema',
                'initialValue',
                'title',
                'theme',
                'icon',
                'locale',
                'readonly',
                'required',
                'monacoEditor'
            ]
        })
    ], AnyEditor);
    return AnyEditor;
}(Vue));
export { AnyEditor };
