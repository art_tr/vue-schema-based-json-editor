import Vue from 'vue';
import * as common from 'schema-based-json-editor';
export declare class AnyEditor extends Vue {
    schema: common.StringSchema;
    initialValue?: string;
    title: string;
    theme: common.Theme;
    icon: common.Icon;
    locale: common.Locale;
    readonly: boolean;
    required: boolean;
    private hasDeleteButton;
    monacoEditor?: common.MonacoEditor;
    value?: common.ValueType;
    private monacoCodeEditor?;
    beforeMount(): void;
    mounted(): void;
    get className(): string;
    get titleToShow(): string;
    get isReadOnly(): boolean | undefined;
    get hasDeleteButtonFunction(): boolean;
    toggleOptional: () => void;
}
